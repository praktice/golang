package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

// x int, y int OR x, y int
func add(x, y int) int {
	return x + y
}

// Multi result output
func swap(x, y string) (string, string) {
	return y, x
}

// Named return value
func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

// Variables
var c, python, java bool

// Var with initializer
var i, j int = 1, 2

func main() {
	fmt.Println("Hello, World")
	fmt.Println("Time is", time.Now())
	fmt.Println("Random number", rand.Intn(10))
	fmt.Printf("Square Root of %g is \n", math.Sqrt(7))
	fmt.Println(math.Pi)
	fmt.Println(add(22, 22))

	a, b := swap("Multi", "Output")
	fmt.Println(a, b)
	fmt.Println(split(17))

	var i int
	fmt.Println(i, c, python, java)
	var d, e, f = true, false, "no!"
	fmt.Println(d, e, f)

	// Var short declaration is :=
	k := 33
	fmt.Println(k)

	// Types
	var ii int
	var ff float64
	var bb bool
	var ss string
	fmt.Printf("%v %v %v %q\n", ii, ff, bb, ss)

	// Type Conversion (Long Way)
	//var i int = 42
	//var f float64 = float64(i)
	//var u uint = uint(f)

	// Type Conversion (Short Way)
	xi := 42
	xf := float64(xi)
	xu := uint(xf)
	fmt.Println(xi, xf, xu)

	// Constants
	const World = "CONSTANT WORLD"
	fmt.Println("Constant: ", World)
}
