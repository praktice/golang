package main

import "fmt"

func main() {
  if 3 % 2 == 0 {
    fmt.Println("3 is event")
  } else {
    fmt.Println("3 is odd")
  }

  if 8%4 == 0 {
    fmt.Println("8 is divisble by 4")
  }

  if num := 9; num < 0 {
    fmt.Println("is negative")
  } else if num < 10 {
    fmt.Println("has 1 digit")
  } else {
    fmt.Println("has multiple digits")
  }
}
