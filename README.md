# Golang

- [Golang](#golang)

---

## New Project

```sh
go mod init jream/project
echo -e 'package main\n\nimport "fmt"\n\nfunc main() {\n    fmt.Println("Hello")\n}\n' > "hello.go"

go install
(or)
go install .
(or)
go install jream/project
```

> This will install the binary at `$HOME/go/bin/jream/hello`

To change where compile happens you can set: `go env -w GOBIN=/somewhere/else/bin`

To run binaries easy we can append to PATH:

```sh
export PATH=$PATH:$(dirname $(go list -f '{{.Target}}' .))
```

## Run Only

```sh
go run file.go
```

## Build/Compile

```sh
go build file.go
```

## Clear Cache

Module dependencies are downloaded to `$GOPATH/pkg/mod`

```sh
go clean -modcache
```
