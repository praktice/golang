package main

import (
	"fmt"
	"github.com/google/go-cmp/cmp"
	"morestrings"
)

func main() {
	fmt.Println(morestrings.ReverseRunes("!oG, olleH"))
	fmt.Println(cmp.Diff("Hello World", "Hello Go"))
}

func logic() {
	// if statement
	if a := 1; a > 0 {
		return a
	}

	// for loop
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
	}

	// for loop array, slice, string, map
	for key, value := range oldMap {
		newMap[key] = value
	}

	// Get the Key/Index
	for key := range m {
		if key.expired() {
			delete(m, key)
		}
	}

	// Get the Value
	sum := 0
	for _, value := range array {
		sum += value
	}

	// Case/Switch
	switch {
	case c > 0:
		return 0
	case c > 5:
		return 5
	}
	return 0

}

// Multiple Return
func multireturn() {
	return 1, 2
}
