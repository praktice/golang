# Go Basics

## Concepts

## Variables

## Built in Types

## Operators

## If/Else

## Switch/Case

## Loops

### For

### Iterables, For In

### While

## Classes

## Methods

### Setter/Getters

### Abstract Methods

### Abstract Classes

### Interfaces

### Extend

## Test

### Assert

### Exception

### Try/Catch

---


