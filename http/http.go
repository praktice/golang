package main

// Just learning, NOT that GOOD :)
// go build http.go && ./http.go http://jream.com .js

import (
	"fmt"
	"net/http"
	"os"
	"regexp"
)

func findLinkType(response string, fileExt string) {
	re := regexp.MustCompile(fmt.Sprintf("(\\.%d)", fileExt))
	matches := re.FindAllString(string(response), -1)
	if matches == nil {
		fmt.Println("No matches.")
	} else {
		for _, match := range matches {
			fmt.Println(match)
		}
	}
}

func main() {

  if len(os.Args) != 2 {
    fmt.Fprintf(os.Stderr, "Please provide as follows (http://site.tld): %s URL\n %s File Ext", os.Args[0], os.Args[1])
		os.Exit(1)
	}

  response, err := http.Get(os.Args[1])
  contentStr := string(response)
  findLinkType(response, '.js')
}
